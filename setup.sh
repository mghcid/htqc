#!/bin/bash -e

pip install --upgrade pip
pip install virtualenv
virtualenv venv/
source venv/bin/activate
pip install -r requirements.txt
python setup.py develop
