
##Overview:##

The HTQC is a command-line tool to generate QC statistics high-throughput data. Currently support coverage stats for BAM files given a BED file of targets or a list of gene HUGO symbols and a QC tool for RNA bam files in our fusion pipelines. Later, we will add a QC support for VCF files.

![Alt text](https://bitbucket.org/mghcid/htqc/raw/master/docs/htqc_overview.png)

##How to install:##

Running setup.py via pip should install few dependencies automatically (stored in requirements.txt).

```console
git clone git@bitbucket.org:mghcid/htqc.git
cd htqc
pip install -r requirements.txt .
```

Additionally, there is one python library that need to downloaded and installed manually (bx-python):

bitbucket.org/salturki/bx-python

##Usage:##

After installing the tool, you can display the help message by entering just the name of the tool (or use --help)

```console
htqc
```
This generates the following help message 

```console
Usage: htqc [OPTIONS] COMMAND [ARGS]...

  HTQC is a command-line tool to generate QC statistics for high-throughput
  data (currently BAM and work is in progress for RNA or VCF files).

  Version: v0.2 (2015)

Options:
  --help  Show this message and exit.

Commands:
  coverage  [BAM] Coverage stats based on a BED file or...
  rnaqc     [BAM] RNA-Seq QC stats
  variants  [VCF] General variant stats [not implemented yet]
```

##Coverage QC tool for intervals (e.g. exons) ##

This is a sub-command ```htqc coverage``` to generate a coverage from a BAM file based on intervals stored in a bed file. To show its arguments:

```bash
htqc coverage --help
```

* --stats-name [mean|median] is the name of statistics for calculating the  average number of reads. If your goal is to 
use the output for CNV pipeline normalization, then use the median otherwise the default is using mean (from numpy library).

* --mmq INTEGER    Minimum mapping quality where default is 20

* --mdp INTEGER    Minimum depth where default is 20

* --bed-path  PATH is a path to bed file. For example, this bed file has three hotspot variants:

```text
12	25380288	25380289	KRAS_c.169G_(p.D57)	0	.
12	25380284	25380285	KRAS_c.173C_(p.T58)	0	.
12	25380281	25380283	KRAS_c.175-176_(p.A59)	0	.
```
The last two columns are ignored but are necessary to maintain a valid BED format. 

* --bam-path is a path to bam file

An example of full command line is (example files are in tests folder):

```bash
htqc coverage --bam-path kras.bam -bed-path kras.bed --stats-name mean
```

Which outputs 

```console
#CHROM	START	END	NAME	SIZE (bp)	MEAN_ABS_COVERAGE	MEAN_FILTERED_COVERAGE	MEAN_COLLAPSED_COVERAGE	FRACTION_WITH_ZERO_DP	FRACTION_WITH_MIN_DP
12	25380288	25380289	KRAS_c.169G_(p.D57)	1	248.0	201.0	134.0	0.0	1.0
12	25380284	25380285	KRAS_c.173C_(p.T58)	1	245.0	198.0	133.0	0.0	1.0
12	25380281	25380283	KRAS_c.175-176_(p.A59)	2	242.0	195.5	130.5	0.0	1.0
```

Where:

* ```MEAN_ABS_COVERAGE``` is the mean average coverage per interval (raw counts, unfiltered reads)
* ```MEAN_FILTERED_COVERAGE``` is the mean average coverage per interval if read has mapping quailty > mmq and depth > mdp.
* ```MEAN_COLLAPSED_COVERAGE``` is the same as MEAN_FILTERED_COVERAGE but also aware of pair-end reads (counts a pair once)
* ```FRACTION_WITH_ZERO_DP``` is the fraction of the interval with the bases that have 0 reads
* ```FRACTION_WITH_MIN_DP``` is the fraction of the interval with the bases that have N reads > mdp argument (default is 20x) 

##Coverage QC tool for genes and transcripts ##

Another way to generate coverage table is by providing a list of genes symbols. For example:

```bash
htqc coverage -i sample.bam -g genes.txt
```
The `genes.txt` file contents must be one gene per line. For example:

```text
KRAS
NRAS
PTPN10
```

The output is 

```console
#CHROM	START	END	GENE	TRANSCRIPT	EXON	SIZE (bp)	AVG_COVERAGE	BASES_WITH_0_COVERAGE
12	25398207	25398318	KRAS	NM_033360	exon_2	111	160	0	0
12	25380167	25380346	KRAS	NM_033360	exon_3	179	144	0	0
12	25378547	25378707	KRAS	NM_033360	exon_4	160	154	0	0
12	25368374	25368494	KRAS	NM_033360	exon_5	120	66	0	0
12	25398207	25398318	KRAS	NM_004985	exon_2	111	160	0	0
12	25380167	25380346	KRAS	NM_004985	exon_3	179	144	0	0
12	25378547	25378707	KRAS	NM_004985	exon_4	160	154	0	0
12	25362728	25362845	KRAS	NM_004985	exon_5	117	81	0	0
```

Same columns as before but with the addition of the gene name, transcript id from RefGene and the number of exons. Only coding exons are included. UTR regions are trimmed out.


##Coverage QC tool with bases for hotspots + read counts per position##

Sometimes we need more details about a given hotspots. We need to know the raw coverage and then how many reads support A,C,G,T or N (on the forward and / or reverse strand). As an example (see examples in tests/files folder):
 
```bash
htqc coverage \
--bam-path kras.bam \
--bed-path kras.bed \
--count-bases \
--output-dir temp \ 
--igv-bin XXX 
```

where ```XXX```  is the full path to igv.sh file that comes as part of IGV package. HTQC will use ```igv.sh``` to run a IGV in a batch mode in order to save screen prints of every interval or hotspot inside the bed file as PNG image and save them to the output directory. Inside a ```temp``` folder you will find two things (htqc_coverage_table.txt and *.png images generated by IGV).

Generates the following coverage table

```text
#CHROM	START	END	NAME	SIZE (bp)	MEAN_ABS_COVERAGE	MEAN_FILTERED_COVERAGE	MEAN_COLLAPSED_COVERAGE	FRACTION_WITH_ZERO_D	FRACTION_WITH_MIN_DP	A_RAW	A_HMQ_plus	A_HMQ_minus	C_RAW	C_HMQ_plus	C_HMQ_minus	G_RAW	G_HMQ_plus	G_HMQ_minus	T_RAW	T_HMQ_plus	T_HMQ_minus	N_RAW	N_HMQ_plus	N_HMQ_minus
12	25380288	25380289	KRAS_c.169G_(p.D57)	1	248.0	201.0	134.0	0.0	1.0	0	0	0	1	1	0	0	0	0	247	98	102	0	0	0
12	25380284	25380285	KRAS_c.173C_(p.T58)	1	245.0	198.0	133.0	0.0	1.0	0	0	0	0	0	0	0	0	0	245	98	100	0	0	0
12	25380281	25380283	KRAS_c.175-176_(p.A59)	2	242.0	195.5	130.5	0.0	1.0	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA	NA
```

For each SNV (1bp) there are new columns that hold the read counts for each possible nucleotide A,C,G,T or N. Each nucleotide has three columns. For example for A nucleotide:

A_RAW --> is the raw count of reads with base A at that position. No filters of any kind is applied.
A_HMQ_plus --> is the count of high quality reads on the forward strand (quality > 20 by default, override this value with --mpq argument)
A_HMQ_mius --> is the count of high quality reads on the reverse strand (quality > 20 by default, override this value with --mpq argument)

Notice the third variant was an INDEL so the tool added 'NA' value instead of reads count since ```--count-bases``` mode supports SNVs only.


##RNA QC tool##

This tool output global stats for RNA/DNA and meant for use with fusion pipelines. 

```bash
htqc rnaqc -i sample.bam -g refGene
```

This will output the following:

```text
#### GLOBAL STATS #####
#GROUP	NON-DUP	NON-DUP%	DUP	DUP%	TOTAL	TOTAL%
total_reads	468106	100.00%	992530	100.00%	1460636	100.00%
read1	120681	25.78%	609637	61.42%	730318	50.00%
read2	347425	74.22%	382893	38.58%	730318	50.00%
mapped	458951	98.04%	992530	100.00%	1451481	99.37%
unmapped	9155	1.96%	0	0.00%	9155	0.63%
split	402784	86.05%	245736	24.76%	648520	44.40%
unsplit	56167	12.00%	746794	75.24%	802961	54.97%


#### JUNCTION STATS ####
#GROUP	NON-DUP	NON-DUP%	DUP	DUP%	TOTAL	TOTAL%
rna	384978	46.01%	326508	23.18%	711486	31.69%
dna	383075	45.78%	751820	53.38%	1134895	50.55%
rna/dna	1.0	-	0.43	-	0.63
u-u	44449	5.31%	27008	1.92%	71457	3.18%
x-x	7138	0.85%	52199	3.71%	59337	2.64%
e-e	17059	2.04%	250817	17.81%	267876	11.93%
e-i	153541	18.35%	261245	18.55%	414786	18.48%
e-j	218240	26.08%	158057	11.22%	376297	16.76%
i-e	152652	18.24%	201486	14.31%	354138	15.77%
i-i	49177	5.88%	183599	13.04%	232776	10.37%
i-j	23951	2.86%	50631	3.60%	74582	3.32%
j-e	147546	17.63%	132733	9.42%	280279	12.48%
j-i	3754	0.45%	54859	3.90%	58613	2.61%
j-j	19192	2.29%	35718	2.54%	54910	2.45%


#### GENE STATS #####
#Gene	e-e (NONDUP)	e-i (NONDUP)	e-j (NONDUP)	i-e (NONDUP)	i-i (NONDUP)	i-j (NONDUP)	j-e (NONDUP)	j-i (NONDUP)	j-j (NONDUP)	e-e (DUP)	e-i (DUP)	e-j (DUP)	i-e (DUP)	i-i (DUP)	i-j (DUP)	j-e (DUP)	j-i (DUP)	j-j (DUP)
ARHGAP6	4	39	425	0	32	0	16	6	0	204	654	395	0	21	0	310	68	0
```

Where

* *e* is exon
* *i* is intron
* *j* is junction of an exon
* *u* is unknown or unmapped
* *x* is reads mapped to chromosomes not in 1..22, X or Y

##VCF QC tool##
Not implemented yet.