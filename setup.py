"""
Project: htqc
File: setup


Contact:
---------
salturki@gmail.com

10:41 
5/7/15
"""
__author__ = 'Saeed Al Turki'

from setuptools import setup, find_packages
exec(open('htqc/version.py').read())


def load_requirements():
    req_list = []
    with open('requirements.txt', 'r') as fileobj:
        for line in fileobj:
            if line.startswith('-e '):
                continue
            req_list.append(line.strip())
    return req_list


setup(
    name='htqc',
    version=__version__,
    py_modules=['htqc'],
    packages=find_packages(),
    include_package_data=True,
    url='https://bitbucket.org/mghcid/htqc',
    author='Saeed Al Turki',
    author_email='salturki@gmail.com',
    license='MIT',
    install_requires=load_requirements(),
    dependency_links=['https://bitbucket.org/salturki/bx-python/get/master.zip#egg=bx-python'],
    entry_points='''
        [console_scripts]
        htqc=htqc.main:cli
    ''',
)
