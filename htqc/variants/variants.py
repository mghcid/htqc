"""
Project: htqc
File: variants


Contact:
---------
salturki@gmail.com

11:22
6/8/15
"""
from __future__ import print_function

import click


@click.group(invoke_without_command=True)
@click.option('--input-vcf', '-i', help='Input file (bam)')
def cli(input_vcf=None):
    """[VCF] General variant stats [pending]"""
    if input_vcf is None:
        raise ValueError('input_vcf argument is missing.')
    print(input_vcf)


if __name__ == '__main__':
    cli()  # cli: command line interface
