"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import re
from itertools import izip, chain, repeat
import logging

logger = logging.getLogger(__name__)

#########################################
# Code here is based on Rover-PCR caller
# https://github.com/bjpop/rover
#########################################


class Base(object):
    """ A DNA base paired with its quality score."""

    def __init__(self, base, qual):
        self.base = base  # a string
        self.qual = qual  # an int

    def as_tuple(self):
        """ Return the base and quality score of the base as a tuple."""
        return self.base, self.qual

    def __eq__(self, other):
        return self.as_tuple() == other.as_tuple()

    def __str__(self):
        return str(self.as_tuple())

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.as_tuple)


class SNV(object):
    """ Single nucleotide variant. Bases are represented as DNA strings."""

    def __init__(self, chrom, pos, ref_base, seq_base):
        self.chrom = chrom
        self.pos = pos
        self.vcf_pos = pos - 1
        self.ref_base = ref_base
        self.seq_base = seq_base
        self.ref_seq = ref_base
        self.alt_seq = seq_base
        self.raw_count = 0
        self.plus_count = 0
        self.minus_count = 0
        self.qual = '.'
        self.filter_reason = None
        self.info = []

    def __str__(self):
        return "S: {} {} {} {}".format(self.chrom, self.pos, self.ref_base, self.seq_base)

    def __repr__(self):
        return str(self)

    def as_tuple(self):
        """ Return information about the SNV as a 4-tuple."""
        return self.chrom, self.pos, self.ref_base, self.seq_base

    def __hash__(self):
        return hash(self.as_tuple())

    def __eq__(self, other):
        return self.as_tuple() == other.as_tuple()

    def ref(self):
        """ REF base."""
        return self.ref_base

    def alt(self):
        """ ALT base."""
        return self.seq_base

    def fil(self):
        """ Return "PASS" if the SNV is not filtered, or the reason for
        being discarded otherwise."""
        if self.filter_reason is None:
            return "PASS"
        return self.filter_reason[1:]

    def position(self):
        """ SNV POS."""
        return self.pos


class Insertion(object):
    """ Insertion. Bases are represented as DNA strings."""

    def __init__(self, chrom, pos, inserted_bases, context):
        self.chrom = chrom
        self.pos = pos
        self.vcf_pos = pos - 1
        self.inserted_bases = inserted_bases
        self.qual = '.'
        self.filter_reason = None
        self.info = []
        self.ref_seq = context
        self.alt_seq = context + inserted_bases
        self.raw_count = 0
        self.plus_count = 0
        self.minus_count = 0
        self.context = context
        if self.context is None:
            self.info.append("BS=T")
            self.context = '-'
        elif self.context == 'S':
            self.info.append("SC=T")
            self.context = '-'
        elif self.context == 'H':
            self.info.append("HC=T")
            self.context = '-'

    def __str__(self):
        return "I: {} {} {}".format(self.chrom, self.pos, self.inserted_bases)

    def __repr__(self):
        return str(self)

    def as_tuple(self):
        """ Return information about the insertion as a 3-tuple."""
        return self.chrom, self.pos, self.inserted_bases

    def __hash__(self):
        return hash(self.as_tuple())

    def __eq__(self, other):
        return self.as_tuple() == other.as_tuple()

    def ref(self):
        """ REF base."""
        return self.context

    def alt(self):
        """ ALT (inserted) bases."""
        return self.context + self.inserted_bases

    def fil(self):
        """ Return "PASS" if the Insertion is not filtered, or the reason for
        being discarded otherwise."""
        if self.filter_reason is None:
            return "PASS"
        return self.filter_reason[1:]

    def position(self):
        """ Insertion POS."""
        return self.pos - 1


class Deletion(object):
    """ Deletion. Bases are represented as DNA strings."""

    def __init__(self, chrom, pos, deleted_bases, context):
        self.chrom = chrom
        self.pos = pos
        self.vcf_pos = pos - 1
        self.deleted_bases = deleted_bases
        self.qual = '.'
        self.filter_reason = None
        self.info = []
        self.context = context
        self.ref_seq = context + deleted_bases
        self.alt_seq = context
        self.raw_count = 0
        self.plus_count = 0
        self.minus_count = 0
        if self.context is None:
            self.info.append("BS=T")
            self.context = '-'
        elif self.context == 'S':
            self.info.append("SC=T")
            self.context = '-'
        elif self.context == 'H':
            self.info.append("HC=T")
            self.context = '-'

    def __str__(self):
        return "D: {} {} {}".format(self.chrom, self.pos, self.deleted_bases)

    def __repr__(self):
        return str(self)

    def as_tuple(self):
        """ Return infromation about the deletion as a 3-tuple."""
        return self.chrom, self.pos, self.deleted_bases

    def __hash__(self):

        return hash(self.as_tuple())

    def __eq__(self, other):
        return self.as_tuple() == other.as_tuple()

    def ref(self):
        """ REF (deleted) bases."""
        return self.context + self.deleted_bases

    def alt(self):
        """ ALT base."""
        return self.context

    def fil(self):
        """ Return "PASS" if the Deletion is not filtered, or the reason for
        being discarded otherwise."""
        if self.filter_reason is None:
            return "PASS"
        return self.filter_reason[1:]

    def position(self):
        """ Deletion POS."""
        return self.pos - 1


class MdMatch(object):
    """ An element of the md string corresponding to a match."""

    def __init__(self, size):
        self.size = size

    def __str__(self):
        return str(self.size)

    def __repr__(self):
        return self.__str__()


class MdMismatch(object):
    """ An element of the md string corresponding to a mismatch."""

    def __init__(self, ref_base):
        self.ref_base = ref_base

    def __str__(self):
        return self.ref_base

    def __repr__(self):
        return self.__str__()


class MdDeletion(object):
    """ An element of the md string corresponding to a deletion."""

    def __init__(self, ref_bases):
        self.ref_bases = ref_bases

    def __str__(self):
        return "^" + self.ref_bases

    def __repr__(self):
        return self.__str__()


def ascii_to_phred(ascii):
    """ SAM/BAM files store the quality score of a base as a byte (ascii
    character) in "Qual plus 33 format". So we subtract off 33 from the ascii
    code to get the actual score.
    See: http://samtools.sourceforge.net/SAMv1.pdf
    ASCII codes 32 an above are the so-called printable characters, but 32
    is a whitespace character, so SAM uses 33 and above."""
    return ord(ascii) - 33


def make_base_seq(name, bases, qualities):
    """ Take a list of DNA bases and a corresponding list of quality scores
    and return a list of Base objects where the base and score are
    paired together."""
    num_bases = len(bases)
    num_qualities = len(qualities)
    if num_bases <= num_qualities:
        return [Base(b, ascii_to_phred(q)) for (b, q) in izip(bases, qualities)]
    logging.warning("In read {} fewer quality scores {} than bases {}".format(name, num_qualities, num_bases))
    # we have fewer quality scores than bases
    # pad the end with 0 scores (which is ord('!') - 33)
    return [Base(b, ascii_to_phred(q)) for (b, q) in izip(bases, chain(qualities, repeat('!')))]


def proportion_overlap(block_start, block_end, read):
    """ Compute the proportion of the block that is overlapped by the read
          block_start               block_end
               |-------------------------|
        ^---------------------------^
    read.pos                      read_end
               |--------------------|
         overlap_start        overlap_end
    """
    read_end = read.pos + read.rlen - 1
    if read.rlen <= 0:
        # read is degenerate, zero length
        # treat it as no overlap
        logging.warn("Degenerate read: {}, length: {}".format(read.qname, \
                                                              read.rlen))
        return 0.0
    if read_end < block_start or read.pos > block_end:
        # they don't overlap
        return 0.0
    overlap_start = max(block_start, read.pos)
    overlap_end = min(block_end, read_end)
    overlap_size = overlap_end - overlap_start + 1
    block_size = block_end - block_start + 1
    return float(overlap_size) / block_size


def lookup_reads(min_overlap, bam, chrom, start_col, end_col):
    """ Retrieve the read pairs for one block from a BAM file."""
    total_reads = 0
    overlapping_reads = 0
    read_pairs = {}
    try:
        reads = bam.fetch(chrom, start_col, end_col + 1)
    except ValueError:
        logging.warning("Failed to fetch reads for chrom: {} start: {} end: {}".format(chrom, start_col, end_col + 1))
        reads = []
    for read in reads:
        total_reads += 1
        # only keep reads which overlap with the block region by a
        # certain proportion
        overlap = proportion_overlap(start_col, end_col, read)
        if overlap > min_overlap:
            overlapping_reads += 1
            if read.qname not in read_pairs:
                read_pairs[read.qname] = [read]
            else:
                read_pairs[read.qname].append(read)
    print("Number of reads intersecting block: {}".format(total_reads))
    print("Number of reads sufficiently overlapping block: {}".format(overlapping_reads))
    return read_pairs


def get_md(read):
    """ Retrieves the MD string from a read."""
    for tag, val in read.tags:
        if tag == 'MD':
            return val
    return None


# [0-9]+(([A-Z]|\^[A-Z]+)[0-9]+)*
def parse_md(md_string, result):
    """
    Read one element of the md string
    :param md_string:
    :param result:
    :return:
    """
    if md_string:
        number_match = re.match(r'([0-9]+)(.*)', md_string)
        if number_match is not None:
            number_groups = number_match.groups()
            number = int(number_groups[0])
            md_string = number_groups[1]
            return parse_md_snv(md_string, result + [MdMatch(number)])
    return result


def parse_md_snv(md_string, result):
    """
     Read a single nucleotide variant from the md string.
    :param md_string:
    :param result:
    :return:
    """
    if md_string:
        snv_match = re.match(r'([A-Z])(.*)', md_string)
        if snv_match is not None:
            snv_groups = snv_match.groups()
            ref_base = snv_groups[0]
            md_string = snv_groups[1]
            return parse_md(md_string, result + [MdMismatch(ref_base)])
        return parse_md_del(md_string, result)
    return result


def parse_md_del(md_string, result):
    """
     Read a deletion from the md string.
    :param md_string:
    :param result:
    :return:
    """
    if md_string:
        del_match = re.match(r'(\^[A-Z]+)(.*)', md_string)
        if del_match is not None:
            del_groups = del_match.groups()
            ref_bases = del_groups[0][1:]
            md_string = del_groups[1]
            return parse_md(md_string, result + [MdDeletion(ref_bases)])
    return result


def nts(none_string):
    """ Turns None into an empty string."""
    if none_string is None:
        return ''
    return str(none_string)


def read_variants(qualthresh, chrom, read, aligned_bases, md_string):
    """ Find all the variants in a single read (SNVs, Insertions, Deletions)."""
    pos = read.pos + 1
    cigar = read.cigar
    seq_index = 0
    result = []
    context = None

    while cigar and md_string:
        cigar_code, cigar_segment_extent = cigar[0]
        next_md = md_string[0]
        if cigar_code == 0:
            if isinstance(next_md, MdMatch):
                # MD match
                if next_md.size >= cigar_segment_extent:
                    next_md.size -= cigar_segment_extent
                    if next_md.size == 0:
                        md_string = md_string[1:]
                    context = aligned_bases[seq_index + cigar_segment_extent - 1].base
                    cigar = cigar[1:]
                    pos += cigar_segment_extent
                    seq_index += cigar_segment_extent
                else:
                    # next_md.size < cigar_segment_extent
                    cigar = [(cigar_code, cigar_segment_extent - next_md.size)] + cigar[1:]
                    context = aligned_bases[seq_index + next_md.size - 1].base
                    md_string = md_string[1:]
                    pos += next_md.size
                    seq_index += next_md.size
            elif isinstance(next_md, MdMismatch):
                # MD mismatch
                seq_base_qual = aligned_bases[seq_index]
                seq_base = seq_base_qual.base
                snv = SNV(chrom, pos, next_md.ref_base, seq_base)
                # check if the read base is above the minimum quality score
                if not ((qualthresh is None) or (seq_base_qual.qual > qualthresh)):
                    snv.filter_reason = ''.join([nts(snv.filter_reason), ";qlt"])
                result.append(snv)
                cigar = [(cigar_code, cigar_segment_extent - 1)] + cigar[1:]
                context = next_md.ref_base
                md_string = md_string[1:]
                pos += 1
                seq_index += 1
            elif isinstance(next_md, MdDeletion):
                # MD deletion, should not happen in Cigar match
                # For troubleshooting: print("MD del in cigar match {} {}".format(md_orig, cigar_orig))
                cigar = None
            else:
                # For troubleshooting: print("Unexpected MD code {}".format(md_orig))
                cigar = None
        elif cigar_code == 1:
            # Insertion
            seq_bases_quals = aligned_bases[seq_index:seq_index + cigar_segment_extent]
            seq_bases = ''.join([b.base for b in seq_bases_quals])
            # check that all the bases are above the minimum quality threshold
            insertion = Insertion(chrom, pos, seq_bases, context)
            if not ((qualthresh is None) or all([b.qual >= qualthresh for b in seq_bases_quals])):
                insertion.filter_reason = ''.join([nts(insertion.filter_reason), ";qlt"])
            result.append(insertion)
            cigar = cigar[1:]
            seq_index += cigar_segment_extent
            # pos does not change
        elif cigar_code == 2:
            # Deletion
            if isinstance(next_md, MdDeletion):
                seq_base = aligned_bases[seq_index]
                deletion = Deletion(chrom, pos, next_md.ref_bases, context)
                if not ((qualthresh is None) or (seq_base.qual >= qualthresh)):
                    deletion.filter_reason = ''.join([nts(deletion.filter_reason), ";qlt"])
                result.append(deletion)
                context = next_md.ref_bases[-1]
                md_string = md_string[1:]
                cigar = cigar[1:]
                pos += cigar_segment_extent
                # seq_index does not change
            else:
                # For troubleshooting: print("Non del MD in Del Cigar, md:{0}, cigar:{1}".format(md_orig, cigar_orig))
                cigar = None
        elif cigar_code == 4:
            # soft clipping
            context = 'S'
            md_string = md_string[1:]
            cigar = cigar[1:]
            seq_index += cigar_segment_extent
        elif cigar_code == 5:
            # hard clipping
            context = 'H'
            md_string = md_string[1:]
            cigar = cigar[1:]
        else:
            # For troubleshooting: print("Unexpected cigar code {}".format(cigar_orig))
            cigar = None
    return result
