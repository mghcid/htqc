"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import sys
from collections import namedtuple
import logging

import click
import vcf


logger = logging.getLogger(__name__)


# callers ordered from most trusted to least
ALL_CALLERS = ['gatk', 'lofreq', 'mutect', 'hotspotter', 'mutect2']
SNAPSHOT2_CALLERS = ['lofreq', 'mutect', 'hotspotter', 'gatk', 'mutect2']


# what are the tags the holds AF values for each caller and which column (INFO or FORMAT)

tag_parser = lambda x: x  # most fields don't need parsing, just return each as is

# Define where should we get the DP fields in each caller?
CALLERS_DP_TAGS = {
    'gatk': ('FORMAT', 'gatk_AD', tag_parser),  # 153,3
    'mutect2': ('FORMAT', 'mutect2_AD', tag_parser),
    'lofreq': ('INFO', 'lofreq_DP4', lambda x: (x[0] + x[1], x[2] + x[3])),  # lofreq_DP4=0,0,195,52
    'mutect': ('FORMAT', 'mutect_AD', tag_parser),
    'hotspotter': ('INFO', 'hotspotter_DP', tag_parser)
}


def extract_af(variant, column_name, tag_name, parser):
    """
    Given a pyvcf record and column name (INFO | FORMAT), return the value
    :param variant: pyVcf record
    :param column_name: INFO or FORMAT
    :param tag_name: tag key name
    :param parser: lambda expression to parse the value, if needed
    :return:
    """
    if column_name == 'INFO':
        return parser(variant.INFO[tag_name])
    elif column_name == 'FORMAT':
        # pyvcf data is _Call object which has no method to get the custom tags easily
        tag_value = getattr(variant.samples[0].data, tag_name)
        return parser(tag_value)
    else:
        raise ValueError('Expected INFO or FORMAT columns.')


def get_caller_af(variant, callers):
    """
    Return three values REF_DP, ALT_DP, ALT_AF and primary caller name based on rules of the
    :param variant: pyvcf record (i.e. variant)
    :return: Tuple of four values (
    """
    ref_dp = None
    alt_dp = None
    alt_af = None
    caller = None
    for caller in callers:
        caller_tag = '{0}_DETECT'.format(caller)
        assert caller_tag in variant.INFO, '{0} tag not found in the VCF file. ' \
                                           'Expected a multi-caller VCF file format.'.format(caller_tag)
        if variant.INFO[caller_tag] == 'Y':
            try:
                ref_dp, alt_dp = extract_af(variant, *CALLERS_DP_TAGS[caller])
                alt_af = round(float(alt_dp) / (ref_dp + alt_dp), 3)
            except (TypeError, ValueError, ZeroDivisionError) as e:
                print('- htqc ERROR -\n\ncaller: {}\nvariant: {}\nData: {}\nError: {}'
                      ''.format(caller, variant, variant.samples[0].data, e.message), file=sys.stderr)
            break
    return ref_dp, alt_dp, alt_af, caller


def calculate_af(counts):
    """
    Given counts dictionary from HTQC coverage tool (get_ref_alt_coverage) method, calculate the allele fraction for SNV
    and return NA for indels.
    :param counts: Dictionary
    :return: List of floats or NAs (raw, hq+, hq-)
    """
    ref_counts, alt_counts = counts['ref'], counts['alt']
    if 'NA' in ref_counts or 'NA' in alt_counts:
        return ['NA' for _ in ref_counts]
    return [round(alt_counts[idx] / (float(ref_counts[idx]) + alt_counts[idx]), 3) if ref_counts[idx] != 0 else 0.
            for idx in range(0, len(ref_counts))]


def update_info_htqc(variant, callers):
    """
    Add HTQC coverage for SNV or INDELs if BAM file was supplied
    :param variant: PyVCF's Reader record object
    :return: master_record object after updating the INFO dictionary with HTQC coverage stats
    """
    ref_dp, alt_dp, alt_af, caller = get_caller_af(variant, callers)
    variant.INFO['REF_DP'] = ref_dp
    variant.INFO['ALT_DP'] = alt_dp
    variant.INFO['ALT_AF'] = alt_af
    variant.INFO['PRIMARY_CALLER'] = caller
    return variant


def main(in_vcf_path, out_vcf_path, callers):
    """
    Parse a VCF file and add REF_DP, ALT_DP and ALT_AF to the header and to each variant INFO column.
    :param in_vcf_path: Path to input VCF file
    :param out_vcf_path: Path to output VCF file
    :return: None
    """
    info = namedtuple('Info', ['id', 'num', 'type', 'desc'])  # same as pyvcf's namedtuple used for info column
    # update header with HTQC coverage fields
    with open(in_vcf_path, 'r') as in_vcf_file:
        with open(out_vcf_path, 'w') as out_vcf_file:

            in_reader = vcf.Reader(in_vcf_file)

            htqc_header = {
                'REF_DP': info(id='REF_DP', num=1, type='Integer', desc='Read counts for reference allele'),
                'ALT_DP': info(id='ALT_DP', num=1, type='Integer', desc='Read counts for alternative allele'),
                'ALT_AF': info(id='ALT_AF', num=1, type='Float', desc='The alternative allele fraction'),
            }

            in_reader.infos.update(htqc_header)

            # write results to out_reader
            out_reader = vcf.Writer(out_vcf_file, in_reader)

            for variant in in_reader:
                variant = update_info_htqc(variant, callers)
                out_reader.write_record(variant)


@click.group(invoke_without_command=True)
@click.option('--in-vcf', '-i', type=click.Path(exists=True), help='Path to input VCF file')
@click.option('--out-vcf', '-o', help='Path to output VCF file')
@click.option('--callers', '-c', type=click.Choice(ALL_CALLERS), multiple=True, default=SNAPSHOT2_CALLERS,
              help='Specify which callers to use.\nExample:\npython --in-vcf `pwd` --callers mutect --callers gatk \n'
                   'Default is {}'.format(', '.join(SNAPSHOT2_CALLERS)))
def cli(**kwargs):
    """Annotate VCF with REF_DP, ALT_DP and ALT_AF"""
    main(kwargs['in_vcf'], kwargs['out_vcf'], kwargs['callers'])


if __name__ == "__main__":
    cli()
