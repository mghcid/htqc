"""
Project: htqc
File: test_ucsc


Contact:
---------
salturki@gmail.com

09:43
6/18/15
"""
from __future__ import print_function
from __future__ import absolute_import

from nose.tools import assert_equal, assert_raises

from htqc.database.ucsc import UCSCTable


class TestUCSCTable(object):
    """Unit testing class for Coverage module"""

    @classmethod
    def setup_class(cls):
        """This method is run once for each class before any tests are run"""
        cls.ucsc_refgene_table = UCSCTable(table_name='refGene')
        cls.ucsc_ensembl_table = UCSCTable(table_name='wgEncodeGencodeBasicV19')

    def test_ucsc_table_name(self):
        """Test allowed table names from UCSC"""
        assert_raises(ValueError, self.ucsc_refgene_table.table_name_is_allowed, 'kgXref')
        assert_equal(self.ucsc_refgene_table.table_name_is_allowed('refGene'), 'refGene')
        assert_equal(self.ucsc_refgene_table.table_name_is_allowed('wgEncodeGencodeBasicV19'), 'wgEncodeGencodeBasicV19')

    def test_transcripts(self):
        """Test transcript queries"""
        assert_equal(self.ucsc_ensembl_table.get_transcripts('NOTCH1')[0]['name'], 'ENST00000277541.6')
        assert_equal(self.ucsc_refgene_table.get_transcripts('NOTCH1')[0]['name'], 'NM_017617')

    def test_exons(self):
        """Test exon queries"""
        assert_equal(len(self.ucsc_refgene_table.get_exons('NM_017617')), 34)
        assert_equal(len(self.ucsc_ensembl_table.get_exons('ENST00000277541.6')), 34)
