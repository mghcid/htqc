"""
Project: htqc
File: test_coverage


Contact:
---------
salturki@gmail.com

15:32
6/9/15
"""

from __future__ import print_function

import os
import sys

from nose.tools import assert_equal, assert_true

from htqc.bam.coverage import Coverage


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


class FakeRead(object):
    """ Fake pysam AlignedRead object for testing """
    def __init__(self):
        """init"""
        self.qname = 'FakeRead'
        self.tags = [('MG', '????'), ('RG', 'chr-pos-tlen-barcode')]

class TestCoverage(object):
    """Unit testing class for Coverage module"""
    @classmethod
    def setup_class(cls):
        """This method is run once for each class before any tests are run"""
        cls.bam_path = os.path.join(__location__, 'files', 'kras.bam')
        cls.bed_path = os.path.join(__location__, 'files', 'kras.bed')
        cls.genes_path = os.path.join(__location__, 'files', 'genes.txt')
        cls.cov_bed = Coverage(bam_path=cls.bam_path, bed_path=cls.bed_path)
        cls.cov_gene = Coverage(bam_path=cls.bam_path, genes_path=cls.genes_path)

    @classmethod
    def teardown_class(cls):
        """This method run once for each class _after_ all tests are run"""

    def setup(self):
        """This method run once before _each_ test method is executed"""

    def teardown(self):
        """This method run once after _each_ test method is executed"""

    def test_init(self):
        """Test initialization"""
        assert_equal(self.cov_bed.min_mapping_qual, 20)
        assert_equal(self.cov_bed.ldc, 20)

    def test_load_genes(self):
        """Test gene loading"""
        assert_equal(self.cov_gene.gene_list[0], 'KRAS')

    def test_get_transcripts(self):
        """Querying transcripts by gene name"""
        assert_equal(len(self.cov_gene.get_transcripts('KRAS')), 2)

    def test_coverage_stats(self):
        """Test coverage stats object for a given interval"""
        obs = self.cov_bed.coverage_stats('12', 25380288, 25380289)
        exp = {'absolute_count': 248.0, 'fraction_lte_lower_dp': 0.0,
               'fraction_with_zero_dp': 0.0, 'hq_collapsed_count': 134.0,
               'hq_count': 201.0, 'fraction_gte_upper_dp': 1.0}
        for term in obs:
            assert_equal(obs[term], exp[term])

    def test_parse_transcript(self):
        """Parse transcript record"""
        transcripts = self.cov_gene.get_transcripts('KRAS')
        # 2 transcripts are returned and the order is not deterministic
        assert_true(self.cov_gene.parse_transcript(transcripts[0])[:2] in [('NM_004985', '12'), ('NM_033360', '12')])

    def test_get_barcode(self):
        """Get barcode"""
        read = FakeRead()
        assert_equal('chr-pos-tlen-barcode', self.cov_bed.get_molbc(read))
        assert_equal('barcode', self.cov_bed.get_molbc(read, no_pos=True))

    def test_read_interval_overlap(self):
        """Check the overlapping coordinates between a read and an interval"""
        reads = self.cov_bed.bam.fetch('12', 25380288, 25380289)
        read = None
        for read in reads:
            break  # pick up the first read
        # The first read length is 100bp and start position 25380189. Now , testing various overlapping scenarios
        interval = (25380200, 25380300)
        assert_equal(self.cov_bed.read_interval_overlap(interval[0], interval[1], read), (25380200, 25380289))

        interval = (25380100, 25380300)
        assert_equal(self.cov_bed.read_interval_overlap(interval[0], interval[1], read), (25380189, 25380289))

        interval = (25380200, 25380250)
        assert_equal(self.cov_bed.read_interval_overlap(interval[0], interval[1], read), (25380200, 25380250))

    @staticmethod
    def test_median_mean_stats():
        """Test the median / mean stats"""
        bam_path = os.path.join(__location__, 'files', 'ARHGAP6_RNA.bam')
        gene_path = os.path.join(__location__, 'files', 'genes.txt')

        # test median
        coverage_median = Coverage(bam_path=bam_path, genes_path=gene_path, stats_name='median')
        obs = coverage_median.coverage_stats('X', 11682360, 11682948)
        exp = {'absolute_count': 0.0, 'fraction_lte_lower_dp': 1.0,
               'fraction_with_zero_dp': 0.84, 'hq_collapsed_count': 0.0,
               'hq_count': 0.0, 'fraction_gte_upper_dp': 0.0}
        for term in obs:
            assert_equal(obs[term], exp[term])

        # test mean
        coverage_mean = Coverage(bam_path=bam_path, genes_path=gene_path, stats_name='mean')
        obs = coverage_mean.coverage_stats('X', 11682360, 11682948)
        exp = {'absolute_count': 35.76, 'fraction_lte_lower_dp': 1.0,
               'fraction_with_zero_dp': 0.84, 'hq_collapsed_count': 0.9,
               'hq_count': 1.8, 'fraction_gte_upper_dp': 0.0}
        for term in obs:
            print(term)
            assert_equal(obs[term], exp[term])

    @staticmethod
    def test_check_chr_prefix_in_bam():
        """Test check_chr_prefix_in_bam to return the proper chr prefix for correct region fetching"""
        bam_path = os.path.join(__location__, 'files', 'example_with_chr.bam')
        coverage_obj = Coverage(bam_path=bam_path, stats_name='mean')
        assert_equal('chr', coverage_obj.check_chr_prefix_in_bam())

        bam_path = os.path.join(__location__, 'files', 'kras.bam')
        coverage_obj = Coverage(bam_path=bam_path, stats_name='mean')
        assert_equal('', coverage_obj.check_chr_prefix_in_bam())

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'debug':
        print('Debug mode.')
        tc = TestCoverage()
        tc.setup_class()
        tc.test_parse_transcript()
        tc.test_get_barcode()
        tc.teardown()
