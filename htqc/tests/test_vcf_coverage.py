"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

from nose.tools import assert_equal
from pkg_resources import resource_filename

import vcf

from htqc.variants.vcf_coverage import calculate_af, get_caller_af, update_info_htqc, ALL_CALLERS


def test_get_caller_af():
    """Unit testing get_caller_af method """
    vcf_path = resource_filename('htqc.tests.files', 'multi_caller.vcf')
    vcf_file = vcf.Reader(open(vcf_path), 'r')

    expected_af = {
        # variant key: ref_dp, alt_dp, alt_af, primary caller
        "1_27023500_T_TACTCCTGGCGGTTATGTCC": (153, 3, 0.019, 'mutect2'),
        "10_43613843_G_T": (0, 213, 1.0, 'gatk'),
        "10_43617439_C_G": (220, 10, 0.043, 'lofreq'),
        "1_27023566_G_C": (140, 13, 0.085, 'mutect'),
    }

    for variant in vcf_file:
        var_key = "{0}_{1}_{2}_{3}".format(variant.CHROM, variant.POS, variant.REF, str(variant.ALT[0]))
        observed_values = get_caller_af(variant, ALL_CALLERS)
        assert_equal(observed_values, expected_af[var_key])


def test_update_info_htqc():
    """Unit testing update_info_htqc method """
    vcf_path = resource_filename('htqc.tests.files', 'multi_caller.vcf')
    vcf_file = vcf.Reader(open(vcf_path), 'r')
    expected = {
        "1_27023500_T_TACTCCTGGCGGTTATGTCC": ['REF_DP=153', 'ALT_DP=3', 'ALT_AF=0.019'],
        "10_43613843_G_T": ['REF_DP=0', 'ALT_DP=213', 'ALT_AF=1.0'],
        "10_43617439_C_G": ['REF_DP=220', 'ALT_DP=10', 'ALT_AF=0.043'],
        "1_27023566_G_C": ['REF_DP=140', 'ALT_DP=13', 'ALT_AF=0.085'],
    }
    for variant in vcf_file:
        var_key = "{0}_{1}_{2}_{3}".format(variant.CHROM, variant.POS, variant.REF, str(variant.ALT[0]))
        updated_variant = update_info_htqc(variant, ALL_CALLERS)
        for tag_value in expected[var_key]:
            tag, value = tag_value.split("=")
            assert_equal(str(updated_variant.INFO[tag]), value)


def test_calculate_af():
    """ Unit testing calculate the allele frequency"""
    counts = {'ref': [16, 10, 6], 'alt': [8, 5, 3]}
    assert_equal(calculate_af(counts), [0.333, 0.333, 0.333])

    counts = {'ref': [8, 0, 8], 'alt': [8, 4, 4]}
    assert_equal(calculate_af(counts), [0.5, 0.0, 0.333])

    counts = {'ref': ['NA', 'NA', 'NA'], 'alt': ['NA', 'NA', 'NA']}
    assert_equal(calculate_af(counts), ['NA', 'NA', 'NA'])
