"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

# from pkg_resources import resource_filename
# from nose.tools import assert_equal
#
# from htqc.bam import rnaqc


# def test_gene_rna_coverage():
#     """Unit testing RNA coverage per gene"""
#     input_bam = resource_filename('htqc.tests.files', 'ARHGAP6_RNA_sortedByName.bam')
#     gene = 'ARHGAP6'
#     rnaqc.is_sorted_by_qname(input_bam)  # check if the bam is sorted by qname not by confidantes
#     rnaqc.load_gene_models('refGene')
#     rnaqc.bam_stat(input_bam, genes_list=None)
#
#     observed_counts = rnaqc.GENES_STATS
#     print("+++", observed_counts[gene])
#     # for non duplicated reads for ARHGAP6 gene
#     expected_counts = {
#         'e-e': 4,
#         'e-i': 39,
#         'e-j': 425,
#         'i-e': 0,
#         'i-i': 32,
#         'i-j': 0,
#         'j-i': 6,
#         'j-e': 16,
#         'j-j': 0,
#     }
#     for item in expected_counts:
#         assert_equal(expected_counts[item], observed_counts[gene][item]['nondup'])
