"""
Project: htqc
File: test_utils


Contact:
---------
salturki@gmail.com

13:07
6/19/15
"""

from __future__ import print_function
from __future__ import absolute_import

import os
import pkg_resources
from nose.tools import assert_equal, assert_raises

import pysam

from htqc import utils
from htqc.bam.coverage import Coverage


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


def test_convert_chrom():
    """ Unit testing convert BAM reference id from integers to strings"""
    assert_equal(utils.convert_chrom(11), '11')
    assert_equal(utils.convert_chrom(23), 'X')
    assert_equal(utils.convert_chrom(24), 'Y')


def test_is_sorted_by_qname():
    """ Unit testing to is_sorted_by_qname """
    path1 = pkg_resources.resource_filename('htqc.tests.files', 'kras.bam')  # sorted by coordinate
    path2 = pkg_resources.resource_filename('htqc.tests.files', 'kras_sorted_by_qname.bam')  # sorted by queryname
    assert_raises(ValueError, utils.is_sorted_by_qname, path1)
    assert_equal(utils.is_sorted_by_qname(path2), True)


def test_read_is_high_quality():
    """Are the read quality stats correct"""
    bam_path = os.path.join(__location__, 'files', 'kras.bam')
    bam = pysam.AlignmentFile(bam_path)
    reads = bam.fetch('12', 25380288, 25380289)
    read = None
    for read in reads:
        break  # pick up the first read
    assert_equal(utils.read_is_low_quality(read), False)


def test_get_variant_coverage():
    """Test the count of ref/alt reads for a SNV / DEL / INS"""
    # SNV synthetic
    bam_path = os.path.join(__location__, 'files', 'kras.bam')
    bed_path = os.path.join(__location__, 'files', 'kras.bed')
    bed_cov = Coverage(bam_path=bam_path, bed_path=bed_path)
    counts = utils.get_variant_coverage(bed_cov.bam, '12', 25368462, 'C', 'T')
    assert_equal(counts['ref'], [0, 0, 0])
    assert_equal(counts['alt'], [80, 46, 34])  # only HQ reads

    # DEL synthetic
    bam_path = pkg_resources.resource_filename('htqc.tests.files', 'del_ins_snv.bam')
    bam = pysam.AlignmentFile(bam_path)
    chrom, start = "1", 1987967
    counts = utils.get_variant_coverage(bam, chrom, start, 'T', 'TGTGCGCGAACAATTC')
    assert_equal(counts['alt'], [11, 4, 7])

    # INS CEBPA real sample
    bam_path = os.path.join(__location__, 'files', 'cebpa.bam')
    cov = Coverage(bam_path=bam_path)
    counts = utils.get_variant_coverage(cov.bam, '19', 33792731, 'G', 'GGCGGGT')
    assert_equal(counts['ref'], [205, 88, 117])
    assert_equal(counts['alt'], [83, 33, 50])

    # DEL CEBPA real sample
    counts = utils.get_variant_coverage(cov.bam, '19', 33792754, 'GGGC', 'G')
    assert_equal(counts['ref'], [199, 87, 112])
    assert_equal(counts['alt'], [4, 1, 3])
