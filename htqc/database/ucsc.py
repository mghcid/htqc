"""
Project: htqc
File: ucsc


Contact:
---------
salturki@gmail.com

09:14
6/18/15
"""
from __future__ import print_function
from __future__ import absolute_import

import os
import sqlite3 as lite
import cruzdb  # pylint: disable=F0401


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

SUPPORTED_TABLES = ['refGene', 'wgEncodeGencodeBasicV19']


class UCSCTable(object):
    """
    A base class of UCSC tables (gene models)
    """
    def __init__(self, genome_name='hg19', table_name='refGene'):
        """
        Initiate the class
        :param genome_name: The genome organism and version. Default is hg19
        :param table_name: UCSC table name. Default is refGene
        :return: None
        """
        self.db_path = os.path.join(__location__, "ucsc.db")
        self.db_conn = lite.connect(self.db_path)
        self.genome_name = genome_name
        self.table_name = self.table_name_is_allowed(table_name)
        self.has_table()
        self.columns = self.get_columns_name()

    @staticmethod
    def table_name_is_allowed(name):
        """
        Checks if the table name is supported or not
        :param name: UCSC table name
        :return: UCSC table name or raises error if not supported.
        """
        if name in SUPPORTED_TABLES:
            return name
        else:
            raise ValueError('The table name "{}" is not supported.'
                             ' Only "{}" tables are supported.'.format(name, ",".join(SUPPORTED_TABLES)))

    def has_table(self):
        """
        Check if the local database has the table, otherwise mirror it from UCSC.
        :return: Boolean
        """
        cur = self.db_conn.cursor()
        cur.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{}';".format(self.table_name))

        if cur.fetchone()[0] == 0:
            self.mirror()
        return True

    def mirror(self):
        """
        Connect to UCSC database and mirror the table to a local SQLite database.
        :return: None
        """
        cruzdb.Genome(self.genome_name).mirror([self.table_name], "sqlite:///" + self.db_path)

    def get_all_genes(self):
        """
        Query gene model and return all genes HUGO symbols.
        :return: A list of genes
        """
        sql = "select name2 from {}".format(self.table_name)
        genes = [x[0] for x in set(self.db_conn.execute(sql).fetchall())]
        return genes

    def get_transcripts(self, gene_name):
        """
        Compile a list of transcript records for a given gene.
        :param gene_name: A string of the gene name (HUGO symbol)
        :return: Dictionary of all transcripts
        """
        sql = "select * from {} where name2='{}'".format(self.table_name, gene_name)
        data = []
        for record in self.db_conn.execute(sql).fetchall():
            data.append(dict(zip(self.columns, record)))
        return data

    def get_exons(self, transcript_id, coding_only=False):
        """
        For a transcript, get all exon pairs in a list of tuples. If coding_only set to True then
        :param transcript_id: RefGene transcript id
        :param coding_only: Boolean
        :return: Exon pairs as a list of tuples
        """
        sql = "select exonStarts, exonEnds, exonFrames from {} where name='{}'".format(
            self.table_name, transcript_id)
        starts, ends, exon_frame = self.db_conn.execute(sql).fetchall()[0]
        exons = zip(starts.strip('"').split(",")[:-1], ends.strip('"').split(',')[:-1])
        if coding_only:
            return exons
        holder = []
        exon_frames = exon_frame.split(',')[:-1]
        for idx, frame in enumerate(exon_frames):
            if frame == '-1':  # no frame
                continue
            holder.append(exons[idx])
        return holder

    @property
    def get_cruzdb(self):
        """
        Return a genome object from Cruzdb for additional features like k-nearest neighbors, upstream,
        and downstream searches.
        :return:
        """
        return cruzdb.Genome(self.db_path)

    def get_columns_name(self):
        """
        Extract the columns name in a table
        :return: List of columns names
        """
        cursor = self.db_conn.execute('select * from {}'.format(self.table_name))
        return [description[0] for description in cursor.description]

    def get_all_records(self):
        """
        Get all records in the table in a generator
        :return: Generator of all records
        """
        sql = "select * from {}".format(self.table_name)
        for record in self.db_conn.execute(sql).fetchall():
            yield record
