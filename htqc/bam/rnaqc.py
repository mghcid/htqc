"""
Project: HTQC
File: rnaqc


Contact:
---------
salturki@gmail.com

11:37
4/3/15
"""

from copy import deepcopy

import click
import pysam
from bx.intervals.intersection import Intersecter, Interval  # pylint: disable=E0611

from htqc.utils import convert_chrom, nice_percentage_string, clean_chrom, is_sorted_by_qname, load_genes
from htqc.database.ucsc import UCSCTable


groups = ['total_tags', 'total_reads', 'read1', 'read2',
          'mapped', 'unmapped', 'split', 'unsplit',
          'x-x', 'u-u', 'dna', 'rna', 'rna/dna']
GLOBAL_STATS = {key: {"dup": 0, "nondup": 0} for key in groups}

JUNCTION_COMBINATIONS = {}  # holder dict to be copied for every gene

for i in ['e', 'i', 'j']:
    for j in ['e', 'i', 'j']:
        key = "%s-%s" % (i, j)
        JUNCTION_COMBINATIONS[key] = {"dup": 0, "nondup": 0}
        GLOBAL_STATS[key] = {"dup": 0, "nondup": 0}


GENES_STATS = {}  # a holder for the read count results per gene
TARGET_GENES = []   # list of genes to limit to coverage analysis
GENE_MODELS = {}  # keys are transcript's start and end position and value are the details of that transcript
GENE_EXONS = {}  # holds unique exons from any transcript of that gene
TRANSCRIPTS = {}  # the keys are Intervals of each transcript
JUNCTIONS = {}  # a dictionary where keys are the start or ends of each exon +/- 1,2,3bp to find j-? or ?-j reads.


def get_longest_transcripts(transcripts):
    """
    Given a list of transcripts intervals and return the longest one
    :param transcripts: A list of Intervals
    :return: Interval object
    """
    max_size = 0
    longest_interval = None
    for interval in transcripts:
        size = abs(interval.start - interval.end)
        if size > max_size:
            max_size = size
            longest_interval = interval
    return longest_interval


def get_unique_exons(transcripts):
    """
    Get all unique exon boundaries of a gene transcripts. This is important because we need to find out any reads
    that overlap exons boundaries in any transcripts.
    :param transcripts: A subset dictionary from TRANSCRIPTS
    :return: List of tuples of unique exons
    """
    unique_exons = Intersecter()
    for transcript in transcripts:
        for exon in transcripts[transcript]['exons']:
            if exon not in unique_exons:
                unique_exons.add_interval(exon)
    return unique_exons


def get_read_junction(reference_id, r_start, r_end):
    """
    For every read in a given interval, determines where does it overlap in the gene model exon-exon , intron-exon,
     etc.) and return both the overlap placement type and the gene symbol.
    :param reference_id: String chromosome id
    :param r_start: Integer the read start
    :param r_end: Integer the read end.
    return: read_place, overlapping items (all gene and transcripts overlapping the boundaries of this read).
    """
    gene_id = None
    r_start_place = 'u'
    r_end_place = 'u'

    if reference_id > 23:
        return 'x-x', gene_id

    chrom = convert_chrom(reference_id + 1)
    # Does the read overlap any transcript?
    transcripts_intervals = TRANSCRIPTS[chrom].find(r_start, r_end)
    if not transcripts_intervals:  # if transcript_intervals is []:
        # This read is not known to overlap with any transcript. return u-u (unknown-unknown) boundaries
        return '{0}-{1}'.format(r_start_place, r_end_place), gene_id

    # if there are multiple transcripts, get the longest
    longest_interval = get_longest_transcripts(transcripts_intervals)
    gene_id = GENE_MODELS[chrom][(longest_interval.start, longest_interval.end)]['gene_name']

    # Does the read's start position overlaps an exon

    exons = GENE_EXONS[gene_id]

    if r_start in JUNCTIONS[chrom]:  # this is a hash table (a dictionary not an interval for quick retrial)
        r_start_place = 'j'
    elif exons:  # if there is exons then the read start overlaps part of the exon
        r_start_place = 'e'
    else:  # not part of JUNCTIONS and doesn't overlap exon but since it overlaps a gene, its start must be intronic
        r_start_place = 'i'

    # do the same for the read's end position
    if r_end in JUNCTIONS[chrom]:
        r_end_place = 'j'
    elif exons:
        r_end_place = 'e'
    else:
        r_end_place = 'i'

    read_place = "%s-%s" % (r_start_place, r_end_place)

    return read_place, gene_id


def sum_up_dna_rna_counts():
    """
    Sum up DNA/RNA counts
    """
    # RNA
    for item in ['j-e', 'e-j', 'j-j']:
        for dup_tag in ['dup', 'nondup']:
            GLOBAL_STATS['rna'][dup_tag] += GLOBAL_STATS[item][dup_tag]

    # DNA
    for item in ['e-i', 'i-e', 'i-i', 'i-j', 'j-i']:
        for dup_tag in ['dup', 'nondup']:
            GLOBAL_STATS['dna'][dup_tag] += GLOBAL_STATS[item][dup_tag]

    # RNA/DNA
    for dup_tag in ['dup', 'nondup']:
        GLOBAL_STATS['rna/dna'][dup_tag] = 0 if float(GLOBAL_STATS['dna'][dup_tag]) == 0 else \
            round(GLOBAL_STATS['rna'][dup_tag] / float(GLOBAL_STATS['dna'][dup_tag]), 2)


def get_rna_dna_ratio_total():
    """
    Return RNA to DNA ratio
    """
    nom = GLOBAL_STATS['rna']['dup'] + GLOBAL_STATS['rna']['nondup']
    dom = GLOBAL_STATS['dna']['dup'] + GLOBAL_STATS['dna']['nondup']
    if dom == 0:
        return 0.0
    return round(nom / float(dom), 2)


def bam_stat(input_bam, genes_list):  # pylint: disable=R0915,R0912
    """
    :param input_bam:
    :param genes_list: List of gene HUGO symbols
    Print various stats tables to STDOUT
    """
    bam = pysam.AlignmentFile(input_bam, 'rb')  # pylint: disable=E1101

    previous_read_name = None
    read1 = None
    read2 = None
    for read in bam.fetch(until_eof=True):

        if read.is_duplicate:
            dup_tag = 'dup'
        else:
            dup_tag = 'nondup'

        if read.qname == previous_read_name and read.is_read1 == read1 and read.is_read2 == read2:
            pass  # ignore from counts so we don't count pair-end reads twice
        else:
            GLOBAL_STATS['total_reads'][dup_tag] += 1
            if not read.is_unmapped:
                if read.has_tag('SA'):
                    # if read.is_secondary:
                    GLOBAL_STATS['split'][dup_tag] += 1
                else:
                    GLOBAL_STATS['unsplit'][dup_tag] += 1

            if read.is_unmapped:
                GLOBAL_STATS['unmapped'][dup_tag] += 1
            else:
                GLOBAL_STATS['mapped'][dup_tag] += 1

            if read.is_read1:
                GLOBAL_STATS['read1'][dup_tag] += 1

            if read.is_read2:
                GLOBAL_STATS['read2'][dup_tag] += 1

        previous_read_name = read.qname
        read1 = read.is_read1
        read2 = read.is_read2

        if not read.is_unmapped:  # and read.has_tag('SA'):
            read_junction, gene_id = get_read_junction(read.reference_id,
                                                       read.reference_start,
                                                       read.reference_end)
            GLOBAL_STATS[read_junction][dup_tag] += 1
            if gene_id:
                GENES_STATS[gene_id][read_junction][dup_tag] += 1
            # if read_junction not in ['u-u', 'x-x']:
            GLOBAL_STATS['total_tags'][dup_tag] += 1

    print "#### GLOBAL STATS #####"
    print "\t".join(["#GROUP", "NON-DUP", "NON-DUP%", "DUP", "DUP%", "TOTAL", "TOTAL%"])
    items = ['total_reads', 'read1', 'read2', 'mapped', 'unmapped', 'split', 'unsplit']
    for item in items:
        sum_count = GLOBAL_STATS[item]['dup'] + GLOBAL_STATS[item]['nondup']
        print "\t".join([str(x) for x in [item,
                                          GLOBAL_STATS[item]['nondup'],
                                          nice_percentage_string(GLOBAL_STATS[item]['nondup'],
                                                                 GLOBAL_STATS['total_reads']['nondup']),
                                          GLOBAL_STATS[item]['dup'],
                                          nice_percentage_string(GLOBAL_STATS[item]['dup'],
                                                                 GLOBAL_STATS['total_reads']['dup']),
                                          sum_count,
                                          nice_percentage_string(sum_count,
                                                                 GLOBAL_STATS['total_reads']['dup'] +
                                                                 GLOBAL_STATS['total_reads']['nondup'])]])

    print "\n\n#### JUNCTION STATS ####"
    print "\t".join(["#GROUP", "NON-DUP", "NON-DUP%", "DUP", "DUP%", "TOTAL", "TOTAL%"])
    sum_up_dna_rna_counts()
    items = ['rna', 'dna', 'rna/dna', 'u-u', 'x-x']
    items.extend(sorted(JUNCTION_COMBINATIONS.keys()))
    for item in items:
        sum_count = GLOBAL_STATS[item]['dup'] + GLOBAL_STATS[item]['nondup']
        if item != 'rna/dna':
            print "\t".join([str(x) for x in [item,
                                              GLOBAL_STATS[item]['nondup'],
                                              nice_percentage_string(GLOBAL_STATS[item]['nondup'],
                                                                     GLOBAL_STATS['total_tags']['nondup']),
                                              GLOBAL_STATS[item]['dup'],
                                              nice_percentage_string(GLOBAL_STATS[item]['dup'],
                                                                     GLOBAL_STATS['total_tags']['dup']),
                                              sum_count,
                                              nice_percentage_string(sum_count,
                                                                     GLOBAL_STATS['total_tags']['dup'] +
                                                                     GLOBAL_STATS['total_tags']['nondup'])]])
        else:
            print "\t".join([str(x) for x in [item,
                                              GLOBAL_STATS[item]['nondup'],
                                              "-",
                                              GLOBAL_STATS[item]['dup'],
                                              "-",
                                              get_rna_dna_ratio_total()]])

    if genes_list:
        print "\n\n#### GENE STATS #####"
        dup_tags = ['nondup', 'dup']
        header = ['#Gene'] + ["{0} ({1})".format(junction, dup_tag.upper())
                              for dup_tag in dup_tags
                              for junction in sorted(JUNCTION_COMBINATIONS)]
        print "\t".join(header)
        for gene in sorted(GENES_STATS):
            if gene in genes_list:
                line = [gene]
                for dup_tag in dup_tags:
                    for junction in sorted(JUNCTION_COMBINATIONS):
                        read_counts = GENES_STATS[gene][junction][dup_tag]
                        line.append(str(read_counts))
                print "\t".join(line)


def load_target_genes(path):
    """
    Parse a gene text file and return a list of genes.
    """
    holder = []
    file_obj = open(path, 'r')
    for line in file_obj:
        if line.startswith("#"):
            continue
        holder.append(line.strip())
    return holder


def load_gene_models(gene_model):
    """
    Load gene model (RefGene)
    """

    table = UCSCTable(table_name=gene_model)  # table_name='refGene'
    records = table.get_all_records()
    for line in records:

        t_id, chrom, t_start, t_end, gene_name = line[1], line[2], int(line[4]), int(line[5]), line[12]
        chrom = clean_chrom(chrom)

        if not t_id.startswith('NM_'):
            continue

        if chrom not in GENE_MODELS:
            GENE_MODELS[chrom] = {}
            TRANSCRIPTS[chrom] = Intersecter()

        TRANSCRIPTS[chrom].add_interval(Interval(t_start, t_end))

        if (t_start, t_end) not in GENE_MODELS[chrom]:
            GENE_MODELS[chrom][(t_start, t_end)] = {}

        GENE_MODELS[chrom][(t_start, t_end)] = {"transcript_id": t_id,
                                                "strand": line[3],
                                                "gene_name": gene_name,
                                                "frames": line[15].split(",")[:-1],  # last comma is empty
                                                "cdsStart": int(line[6]),
                                                "cdsEnd": int(line[7]),
                                                "exons": Intersecter()}
        GENE_EXONS[gene_name] = Intersecter()  # to hold all unique exons from any transcript

        exon_starts = [int(x) for x in line[9].split(",")[:-1]]
        exon_ends = [int(x) for x in line[10].split(",")[:-1]]

        # load exon junctions
        if chrom not in JUNCTIONS:
            JUNCTIONS[chrom] = {}

        # exon_ends[-1] = int(line[7])  # to avoid UTR
        exons = zip(exon_starts, exon_ends)
        for exon in exons:
            GENE_MODELS[chrom][(t_start, t_end)]['exons'].add_interval(Interval(exon[0], exon[1]))
            GENE_EXONS[gene_name].add_interval(Interval(exon[0], exon[1]))
            for idx in range(1, 4):
                # The following four lines add exon with flanking +/- 1bp, 2bp, 3bp on each side.
                # A read that has start or end overlaps with one of these values is flagged as part of j-? or ?-j reads
                # where "?" could be anything else i, e or j
                JUNCTIONS[chrom][exon[0] + idx] = ''
                JUNCTIONS[chrom][exon[0] - idx] = ''
                JUNCTIONS[chrom][exon[1] + idx] = ''
                JUNCTIONS[chrom][exon[1] - idx] = ''

        # create a place holder dictionary to store the read counts per gene
        if gene_name not in GENES_STATS:
            GENES_STATS[gene_name] = deepcopy(JUNCTION_COMBINATIONS)

    return GENE_MODELS, TRANSCRIPTS, JUNCTIONS


@click.group(invoke_without_command=True)
@click.option('--input-bam', '-i', help='Input file (bam)')
@click.option('--gene-model', '-g', default='refGene', help='refGene [default] or wgEncodeGencodeBasicV19')
@click.option('--gene-path', '-l', help='A text file of gene HUGO symbols (one per line) to restrict the QC')
def cli(input_bam=None, gene_model=None, gene_path=None):
    """[BAM] RNA-Seq QC stats"""
    if input_bam is None:
        raise ValueError('input_bam argument is missing.')
    if gene_path:
        genes_list = load_genes(gene_path)
    else:
        genes_list = []
    is_sorted_by_qname(input_bam)  # check if the bam is sorted by qname not by confidantes
    load_gene_models(gene_model)
    bam_stat(input_bam, genes_list)

if __name__ == '__main__':
    cli()
