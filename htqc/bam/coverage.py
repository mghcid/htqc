"""
Project: htqc
File: coverge

Note: pysam count the reads with option 'all' which skip reads in which any of the following
lags are set: BAM_FUNMAP, BAM_FSECONDARY, BAM_FQCFAIL, BAM_FDUP

Contact:
---------
salturki@gmail.com

10:46
5/7/15
"""
from __future__ import print_function

import os
import subprocess as sp

import click
import pysam
import numpy as np
from pybedtools import BedTool

from htqc.database.ucsc import UCSCTable
from htqc.utils import clean_chrom, get_chrom_list, \
    generate_igv_script, generate_writable_file_obj, \
    load_genes, read_is_low_quality

SUPPORTED_STATS_TYPES = {'median': np.median, 'mean': np.mean}
CHROM_LIST = get_chrom_list()
LDC = 50  # the default lower depth cutoff
UDC = 200  # the default upper depth cutoff


class Coverage(object):
    """Outputs coverage stats from BAM files based on target regions in BED or from RefGene"""

    general_stats = {"total_size": 0,
                     "avg_coverage": 0,
                     "uncovered_bases": 0}

    def __init__(self, bam_path, bed_path=None, genes_path=None, count_bases=False,  # pylint: disable=R0913
                 mmq=20, ldc=20, udc=100, stats_name='median', igv_bin=None, output_dir=None,
                 output_file=None, flanking=None, molecular_barcode=False):
        """
        Loads BAM and (BED target regions or Genes list). If genes are supplied then use RefGene model.
        :param bam_path: A file path to a BAM file
        :param bed_path: A file path to a BED file of target regions (if not defined then genes_path must be supplied)
        :param genes_path: A file path to a text of genes one per line (if not defined then bed_path must be supplied)
        :param count_bases:  A flag to count reads supporting ACGT in a BED file
        :param mmq: The minimum mapping quality of reads to consider (default is 20)
        :param ldc: The lower depth cutoff to consider a loci with no bam (default is 20 reads)
        :param udc: The upper depth cutoff to consider a loci with no bam (default is 100 reads)
        :param stats_name: The summary stats of choice is either median or mean
        :param igv_bin: The path to IGV bash script
        :param output_dir: Path to save the igv script to snapshots prints (images)
        :param output_file: Path to save htqc file
        :param flanking: Number of bases to add on both sides of an exon into intronic region.
        :param molecular_barcode: if True removes molecular barcode from read name
        :return: None
        """
        self.bam_path = bam_path
        self.min_mapping_qual = mmq
        self.ldc = ldc
        self.udc = udc
        self.stats_name = stats_name
        self.stats_func = SUPPORTED_STATS_TYPES[stats_name]
        self.bam = pysam.AlignmentFile(bam_path, 'rb')  # pylint: disable=E1101
        self.count_bases = count_bases
        self.igv_bin = igv_bin
        self.output_dir = output_dir
        self.output_file = output_file
        self.flanking = flanking
        self.molecular_barcode = molecular_barcode
        self.chrom_prefix = None
        if bed_path:
            self.bed = BedTool(bed_path)
            if self.igv_bin:
                igv_script_path = generate_igv_script(self.bed, bam_path, output_dir)
                self.run_igv_script(igv_script_path)
        if genes_path:
            self.refgene_model = UCSCTable(table_name='refGene')
            self.gene_list = load_genes(genes_path)

    def run_igv_script(self, igv_script_path):
        """
        If user gives the full path to IGV.sh script, generate IGV batch script and run IGV in order to save screen
        prints to the output directory.
        :param igv_script_path: Full path to igv.sh (tested on Mac and Linux)
        :return: None
        """
        if self.igv_bin is None or not os.path.isfile(self.igv_bin):
            raise ValueError('The path to IGV bin is invalid. Please check the correct path to IGV and try again.')
        sp.call(['bash', self.igv_bin, self.bam_path, '-g', 'hg19', '-b', igv_script_path])
        print("Saved IGV screen prints at {}".format(self.output_dir))

    @staticmethod
    def is_base_clipped(idx, cigar):
        """
        Given the index of the base in a read and the cigar tuples find out if the base is clipped (hard or soft)
        :param idx: intger
        :param cigar:  tuple (0,143), (1, 1) see SAM format: https://samtools.github.io/hts-specs/SAMv1.pdf
        :return: Boolean
        """
        for operation, length in cigar:
            if idx <= length:  # the base index belongs to this bin
                if operation > 2:  # BAM_CSOFT_CLIP is 4 , BAM_CHARD_CLIP is 5
                    return True
            else:
                continue
        return False

    @staticmethod
    def read_interval_overlap(interval_start, interval_end, read):
        """
        Return the start and end coordinates of the overlapping region between a read and its parent interval.
        :param interval_start: Genomic coordinate position for the interval start
        :param interval_end: Genomic coordinate position for the interval end
        :param read: A pysam PileupRead or AlignedSegment object
        :return: A tuple of start and end genomic coordinates
        """
        read_start = read.pos
        read_end = read_start + read.isize
        start = read_start
        end = read_end
        if read_start < interval_start:
            start = interval_start
        if read_end > interval_end:
            end = interval_end
        return start, end

    def check_chr_prefix_in_bam(self):
        """
        Check if the bam file has 'chr' prefix before the chromosome id. If true, return it in order to fetch regions
         correctly otherwise pysam will throw an error because it can't find the chromosome.
        :return:
        """
        if self.chrom_prefix is None:
            header = self.bam.header
            if 'SQ' not in header:
                raise ValueError('The bam header is missing the SQ tag (required to map chromosome names / prefix '
                                 'for proper region fetching')
            first_chrom = self.bam.header['SQ'][0]['SN']  # e,g, header = {'SQ':[{'LN':249250621, 'SN':'chr1}]}
            if first_chrom.lower().startswith('chr'):
                self.chrom_prefix = first_chrom[:3]
            else:
                self.chrom_prefix = ''  # don't add any prefix
        return self.chrom_prefix

    @staticmethod
    def get_molbc(read, no_pos=False):
        """Return molecular barcode from read name
           molecular barcode expected is chr_pos_barcode: 11_12222222_AGAGAGAG
           if no_pos is True, return last piece of data only which is the molecular barcode: AGAGAGAG
        """
        for tag in read.tags:
            if tag[0] == 'RG':
                if no_pos:
                    return tag[1].split('-')[-1]
                return tag[1]
        raise ValueError('Read group tag not found for read {}'.format(read.qname))

    def coverage_stats(self, chrom, start, end):
        """
        For every READ overlapping the interval, count number of overlapping reads with MQ > mmq, return the average
        and number of bases with 0 coverage.
        :param chrom: Chromosome id
        :param start: The interval genomic start
        :param end: The interval genomic end
        :return: A dictionary of coverage stats which include
            "absolute_count" is median nbr of reads without any filters
            "filtered_coverage" is median nbr of reads with MQ > mmq cutoff
            "effective_collapsed_coverage" is median(filtered_coverage after collapsing paired-end reads)
            "nbr_of_bases_with_0_coverage" is total nbr of bases with 0 coverage
            "nbr_of_bases_with_x_coverage" is total of bases with X coverage (default is mdp)
        """
        start -= 1  # since pysam is 0-based index
        end -= 1  # since pysam is 0-based index
        columns = ["absolute_count", "hq_count", "hq_collapsed_count"]
        positions = {k: {'absolute_count': 0, 'hq_count': 0, 'hq_collapsed_count': 0} for k in xrange(start, end)}
        interval_size = float(abs(start - end))
        # holder of reads names to avoid counting paired-end reads twice
        overlapping_read_names = {pos: {} for pos in positions}
        chrom_name = self.check_chr_prefix_in_bam() + clean_chrom(chrom)
        for read in self.bam.fetch(chrom_name, start, end):
            for pos in read.positions:  # xrange(r_start, r_end):
                if pos not in positions:
                    continue
                positions[pos]['absolute_count'] += 1
                if not read_is_low_quality(read, self.min_mapping_qual):
                    positions[pos]['hq_count'] += 1
                    readname = read.qname if not self.molecular_barcode else self.get_molbc(read, no_pos=True)
                    if readname not in overlapping_read_names[pos]:
                        positions[pos]['hq_collapsed_count'] += 1
                        overlapping_read_names[pos][readname] = ''

        # calculate the median for each column
        stats = dict(zip(columns, [round(self.stats_func([positions[pos][col] for pos in positions]), 2)
                                   for col in columns]))

        stats['fraction_with_zero_dp'] = [positions[pos]['hq_count'] for pos in positions].count(0.0) / interval_size

        stats['fraction_lte_lower_dp'] = len([positions[pos]['hq_count'] for pos in positions if
                                              positions[pos]['hq_count'] <= self.ldc]) / interval_size
        stats['fraction_gte_upper_dp'] = len([positions[pos]['hq_count'] for pos in positions if
                                              positions[pos]['hq_count'] >= self.udc]) / interval_size
        return {k: round(v, 2) for k, v in stats.iteritems()}

    def get_transcripts(self, gene_id):
        """
        Query the gene model SQLite database by gene name and return all transcripts and exons
        :param gene_id: Gene HUGO symbol
        :return: A dictionary of all transcripts and exons.
        """
        return self.refgene_model.get_transcripts(gene_id)

    @staticmethod
    def get_header(output_type, stats_name):
        """
        Print the header for the coverage stats.
        :param output_type: bed or gene
        :param stats_name: Mean or median
        :return: None
        """
        if output_type == 'bed':
            return "\t".join(['CHROM', 'START', 'END', 'NAME',
                              'SIZE (bp)',
                              '{}_ABS_COVERAGE'.format(stats_name.upper()),
                              '{}_FILTERED_COVERAGE'.format(stats_name.upper()),
                              '{}_COLLAPSED_COVERAGE'.format(stats_name.upper()),
                              'FRACTION_WITH_ZERO_DP', 'FRACTION_LTE_LOWER_DP', 'FRACTION_GTE_UPPER_DP'])
        elif output_type == 'gene':
            return "\t".join(['CHROM', 'START', 'END', 'GENE', 'TRANSCRIPT', 'EXON',
                              'SIZE (bp)',
                              '{}_ABS_COVERAGE'.format(stats_name.upper()),
                              '{}_FILTERED_COVERAGE'.format(stats_name.upper()),
                              '{}_COLLAPSED_COVERAGE'.format(stats_name.upper()),
                              'FRACTION_WITH_ZERO_DP', 'FRACTION_LTE_LOWER_DP', 'FRACTION_GTE_UPPER_DP'])
        elif output_type == 'hotspot':
            return "\t".join(['CHROM', 'START', 'END', 'NAME',
                              'SIZE (bp)',
                              '{}_ABS_COVERAGE'.format(stats_name.upper()),
                              '{}_FILTERED_COVERAGE'.format(stats_name.upper()),
                              '{}_COLLAPSED_COVERAGE'.format(stats_name.upper()),
                              'FRACTION_WITH_ZERO_DP',
                              'FRACTION_LTE_LOWER_DP',
                              'FRACTION_GTE_UPPER_DP',
                              'A_RAW', 'A_HMQ_plus', 'A_HMQ_minus',
                              'C_RAW', 'C_HMQ_plus', 'C_HMQ_minus',
                              'G_RAW', 'G_HMQ_plus', 'G_HMQ_minus',
                              'T_RAW', 'T_HMQ_plus', 'T_HMQ_minus',
                              'N_RAW', 'N_HMQ_plus', 'N_HMQ_minus'])
        else:
            raise ValueError('output_type argument must be "bed",  "gene" or "hotspot" not "{}"'.format(output_type))

    @staticmethod
    def parse_transcript(record):
        """
        Parse a transcript record from RefGene table in order to extract the transcript_id, chrom and coding exons
        :param record: A row from  table
        :return: transcript_id, chrom, exons
        """
        t_chr = clean_chrom(record['chrom'])
        exons = zip([int(x) for x in record['exonStarts'].strip('"').split(",")[:-1]],
                    [int(x) for x in record['exonEnds'].strip('"').split(",")[:-1]],
                    record['exonFrames'].strip('"').split(',')[:-1])

        for idx, exon in enumerate(exons):
            if exon[2] == '-1':
                continue  # nothing to do. It is already non coding
            if record['cdsStart'] < exon[0] < exon[1] < record['cdsEnd']:
                continue  # this exon doesn't overlap with UTRs
            if exon[0] <= record['cdsStart'] <= exon[1]:
                exons[idx] = (record['cdsStart'], exon[1], exon[2])
                continue
            if exon[0] <= record['cdsEnd'] <= exon[1]:
                exons[idx] = (exon[0], record['cdsEnd'], exon[2])
                continue

        if record['strand'] == '-':
            exons = exons[::-1]

        return record['name'], t_chr, exons

    def bases_counter(self, chrom, start, end):
        """
        For a given position return a dictionary of reads supporting A,C,G,T or N (raw calls and >mmq).
        NOTE: This method supports SNV only (1bp size) not indels.
        :param chrom: String, chromosome id
        :param start: Integer , variant start genomic coordinate
        :param end: Integer , variant end genomic coordinate (should the same as start for SNV).
        :return: Dictionary of counts
        """
        nucleotides = 'ACGTN'
        start -= 1  # since pysam is 0-based index
        end -= 1  # since pysam is 0-based index
        is_snv = start == end - 1
        if is_snv:  # is SNV
            holder = 0
        else:  # indels , cannot handle them yet TODO
            holder = 'NA'

        counts = {'raw': dict(zip(list(nucleotides), [holder for _ in nucleotides])),  # All (unfiltered) reads
                  'hq+': dict(zip(list(nucleotides), [holder for _ in nucleotides])),  # high quality reads on plus
                  'hq-': dict(zip(list(nucleotides), [holder for _ in nucleotides]))}  # high quality reads on minus

        if not is_snv:
            return counts  # return counts with NA values since we can't count bases for INDELs
        chrom_name = self.check_chr_prefix_in_bam() + clean_chrom(chrom)
        for read in self.bam.fetch(chrom_name, start, end):
            for idx, pos in enumerate(read.positions):  # xrange(r_start, r_end):
                if pos != start:
                    continue
                if read.query_alignment_sequence is None:  # avoid reads without query_alignment_sequence
                    continue
                current_read_base = read.query_alignment_sequence[idx]
                counts['raw'][current_read_base] += 1
                if not read.is_duplicate and read.mapq >= self.min_mapping_qual:
                    if read.is_reverse:
                        counts['hq-'][current_read_base] += 1
                    else:
                        counts['hq+'][current_read_base] += 1
        return counts

    def bed_walker(self):
        """
        Parses all rows in a BED file and calculate the average depth for each region.
        :return: Print a table of each target region, the average bam, how many bases not covered.
        """
        if self.output_file:
            file_obj = open(self.output_file, 'w')
        else:
            file_obj = generate_writable_file_obj(self.output_dir, 'htqc_coverage_table.txt')
        if self.count_bases:
            header = self.get_header(output_type='hotspot', stats_name=self.stats_name)
        else:
            header = self.get_header(output_type='bed', stats_name=self.stats_name)

        file_obj.write(header + "\n")

        for interval in self.bed:
            chrom = clean_chrom(interval.chrom)
            if chrom not in CHROM_LIST:
                continue
            start = interval.start
            end = interval.end
            name = interval.name
            stats = self.coverage_stats(chrom, start, end)

            if self.count_bases:  # The user needs base supporting read counts and is a SNV
                base_counts = self.bases_counter(chrom, start, end)
                out_line = [chrom, start, end, name,
                            abs(start - end),
                            stats['absolute_count'],
                            stats['hq_count'],
                            stats['hq_collapsed_count'],
                            stats['fraction_with_zero_dp'],
                            stats['fraction_lte_lower_dp'],
                            stats['fraction_gte_upper_dp']]
                for base in list('ACGTN'):
                    for quality in ['raw', 'hq+', 'hq-']:
                        out_line.append(base_counts[quality][base])
                file_obj.write("\t".join([str(x) for x in out_line]) + "\n")
            else:
                file_obj.write("\t".join([str(x) for x in [chrom, start, end, name,
                                                           abs(start - end),
                                                           stats['absolute_count'],
                                                           stats['hq_count'],
                                                           stats['hq_collapsed_count'],
                                                           stats['fraction_with_zero_dp'],
                                                           stats['fraction_lte_lower_dp'],
                                                           stats['fraction_gte_upper_dp']]]) + "\n")

        print("Saved coverage table at {}".format(file_obj.name))

    def snv_hotspot_walker(self):
        """
        Parses all rows in a BED file of SNV hotspots and calculate the average depth for each region with counts
        of read supporting A,C,G,T or N (raw count and read of MQ > mmq)
        :return: Print a table of each target region, the average bam, how many bases not covered.
        """
        self.get_header(output_type='hotspot', stats_name=self.stats_name)

    def genes_walker(self):
        """
        For each gene in genes_list, get all transcripts and calculate the average for each exon. Print a table of
        gene, transcript, exon boundary and order, size average bam and number of bases with <=1 coverage
        :return: None
        """
        self.get_header(output_type='gene', stats_name=self.stats_name)
        for gene in self.gene_list:
            transcripts = self.get_transcripts(gene)
            for transcript in transcripts:
                t_name, chrom, exons = self.parse_transcript(transcript)
                chrom = clean_chrom(chrom)
                if chrom not in CHROM_LIST:
                    continue
                for idx, exon in enumerate(exons):
                    start, end, frame = int(exon[0]), int(exon[1]), str(exon[2])
                    if frame != '-1':  # -1 indicates a non-coding exon
                        start -= self.flanking
                        end += self.flanking
                        stats = self.coverage_stats(chrom, start, end)
                        print("\t".join([str(x) for x in [chrom, start, end, gene, t_name, "exon_%d" % (idx + 1),
                                                          abs(start - end),
                                                          stats['absolute_count'],
                                                          stats['hq_count'],
                                                          stats['hq_collapsed_count'],
                                                          stats['fraction_with_zero_dp'],
                                                          stats['fraction_lte_lower_dp'],
                                                          stats['fraction_gte_upper_dp']]]))


@click.group(invoke_without_command=True)
@click.option('--bam-path', '-i', required=True, type=click.Path(exists=True), help='A path to BAM file')
@click.option('--bed-path', '-b', type=click.Path(exists=True), help='A path to BED file')
@click.option('--count-bases', '-c', is_flag=True, default=False, help='Output number of reads supporting A,C,G,T,N')
@click.option('--genes-path', '-g', type=click.Path(exists=True), help='A text file with one gene symbol per line')
@click.option('--mmq', '-q', default=20, help='Minimum mapping quality')
@click.option('--ldc', '-d', default=LDC, help='Lower depth cutoff (% of target <= ldp). Default 50')
@click.option('--udc', '-d', default=UDC, help='Upper depth cutoff (% of target >= udp). Default 200')
@click.option('--stats-name', '-s', default='mean', type=click.Choice(['mean', 'median']),
              help='Summary stats median or mean')
@click.option('--output-dir', '-d', help='Path to coverage table and IGV prints (read images)'
                                         ' Works only if a bed-path is supplied')
@click.option('--output-file', '-o', help='Path to coverage table and IGV prints (read images)'
                                          ' Works only if a bed-path is supplied')
@click.option('--igv-bin', '-v', help='Path to IGV igv.sh file')
@click.option('--flanking', '-f', default=20, help='Flanking bases length into intronic region. Default 20')
@click.option('--molecular-barcode', is_flag=True, default=False, help='Removes molecular barcode from read name when '
                                                                       'aggregating counts')
def cli(**kwargs):
    """[BAM] Coverage stats based on a BED file or genes list"""
    if kwargs['bed_path']:
        cov = Coverage(**kwargs)
        cov.bed_walker()
    elif kwargs['genes_path']:
        cov = Coverage(**kwargs)
        cov.genes_walker()
    else:
        msg = 'Error: missing arguments. Please provide a bed file or genes list txt file'
        raise ValueError(msg)

if __name__ == '__main__':
    cli()
