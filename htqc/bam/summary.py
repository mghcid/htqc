"""
(c) MGH Center for Integrated Diagnostics
"""
from __future__ import print_function
from __future__ import absolute_import

import click
import pandas as pd
from htqc.bam.coverage import LDC, UDC


def summarize_hotspot(hotspot_path, output_path):
    """
    Write summary report of HTQC coverage file to the output_path. This report includes:
    - Global coverage
    - % of targets with > 200x
    - % of targets with < 50x
    :param hotspot_path: Path to HTQC coverage file
    :param output_path: Path to write the results
    :return: None
    """
    hotspot_df = pd.DataFrame.from_csv(hotspot_path, sep='\t')
    with open(output_path, 'w') as output_file:
        output_file.write("{1} {0}\n".format('Total nbr of hotspots', len(hotspot_df)))
        output_file.write("{1} {0}\n".format('Nbr of hotspots with zero coverage',
                                             len(hotspot_df[hotspot_df.MEAN_ABS_COVERAGE == 0])))
        output_file.write("{1} {0}\n".format('Hotspots with coverage less than or equal {0}x'.format(LDC),
                                             len(hotspot_df[hotspot_df.MEAN_ABS_COVERAGE <= LDC])))
        output_file.write("{1} {0}\n".format('Hotspots with coverage greater than or equal {0}x'.format(UDC),
                                             len(hotspot_df[hotspot_df.MEAN_ABS_COVERAGE >= UDC])))


def summarize_coverage(coverage_path, output_path):
    """
    Write summary report of HTQC coverage file to the output_path. This report includes:
    - Global coverage
    - % of targets with > 200x
    - % of targets with < 50x
    :param coverage_path: Path to HTQC coverage file
    :param output_path: Path to write the results
    :return: None
    """
    coverage_df = pd.DataFrame.from_csv(coverage_path, sep='\t')
    with open(output_path, 'w') as output_file:
        output_file.write("{0}={1}\n".format('SIZE (bp)', coverage_df['SIZE (bp)'].sum()))
        for col_name in coverage_df.columns[4:]:
            output_file.write("{0}={1}\n".format(col_name, round(coverage_df[col_name].mean(), 3)))


@click.group(invoke_without_command=True)
@click.option('--coverage-path', '-c', type=click.Path(exists=True), help='A path to HTQC coverage file')
@click.option('--hotspot-path', '-h', type=click.Path(exists=True), help='A path to HTQC hotspot file')
@click.option('--output-path', '-o', required=True, type=click.Path(exists=False), help='Output path to write results')
def cli(**kwargs):
    """[HTQC] Summarize HTQC coverage report"""
    if kwargs['coverage_path']:
        summarize_coverage(coverage_path=kwargs['coverage_path'], output_path=kwargs['output_path'])
    elif kwargs['hotspot_path']:
        summarize_hotspot(hotspot_path=kwargs['hotspot_path'], output_path=kwargs['output_path'])
    else:
        print('Error: Missing arguments. Use --help to show the help message.')


if __name__ == '__main__':
    cli()
