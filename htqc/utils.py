"""
Project: htqc
File: utils.py


Contact:
---------
salturki@gmail.com

20:49
6/12/15
"""
from __future__ import print_function

import os
import pysam

from htqc.variants import caller


def convert_chrom(chrom_int, bam_header=None):
    """
    Covert chrom integer id to a string format
    :param chrom_int: Intger
    :param bam_header: Pysam's BAM header dictionary
    """
    if bam_header is None:
        if chrom_int == 23:
            chrom_str = 'X'
        elif chrom_int == 24:
            chrom_str = 'Y'
        else:
            chrom_str = chrom_int
    else:
        chrom_str = bam_header['SQ'][chrom_int]['SN']
    return str(chrom_str)


def get_chrom_list():
    """
    Generate a list of human chromosomes 1..22,X,Y and MT
    :return: List of chromosomes
    """
    chroms = [str(x) for x in range(1, 23)]
    chroms.extend(['X', 'Y', 'MT'])
    return chroms


def clean_chrom(chrom):
    """
    Remove 'chr' or 'Chr' from the chromosome id
    :param chrom: String chromosome
    :return: A string of chromosome id after removing 'chr' if any
    """
    if chrom.lower().startswith('chr'):
        return str(chrom[3:])
    return str(chrom)


def load_genes(path):
    """
    Parses a text file of gene HUGO list names and load it to genes_list variable. Each gene symbol must be in
    one line
    :param path: A path to a string file
    :return: List of genes
    """
    genes = []
    file_obj = open(path, 'r')
    for line in file_obj:
        gene = line.strip().strip(" ")  # strip new line character and white spaces
        if gene != "":
            genes.append(gene)
    file_obj.close()
    return genes


def nice_percentage_string(nom, dom):
    """
    Given a nominator and denominator, returns a float formatted string
    """
    if dom == 0:
        return 0
    return "%.2f%%" % round((nom / float(dom)) * 100, 2)


def is_sorted_by_qname(path):
    """
    Check if the bam file is sorted by qname since we need read pairs in tandem.
    :param path: path to a bam file
    :return: Boolean
    """
    bam = pysam.AlignmentFile(path, 'rb')  # pylint: disable=E1101
    header = bam.header
    if header['HD']['SO'] != 'queryname':
        raise ValueError('The bam file is not sorted by "queryname". '
                         'Please use samtools sort -n to sort the file and try again.')
    bam.close()
    return True


def generate_writable_file_obj(output_dir, file_name):
    """
    Return a python file object with writing pervilages and create the output directory if needed.
    :param output_dir: Path to a bam file
    :param file_name: String file
    :return: Python file object
    """
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    o_path = os.path.join(output_dir, file_name)
    return open(o_path, 'w')


def generate_igv_script(bed, bam_path, output_dir):
    """
    For every region in bed file, generate list of IGV commands in order to instruct IGV to save a snap shot for that
     region and save it as png in the output_dir.
    :param bed:  BedTools object
    :param bam_path: Path to a bam file
    :param output_dir: Path to a directory to save the IGV script and screen prints
    :return: A path to IGV batch script
    """
    file_obj = generate_writable_file_obj(output_dir, 'igv_script.txt')
    file_obj.write('new\n'
                   'snapshotDirectory {0}\n'
                   'genome hg19\n'
                   'load {1}\n'.format(output_dir, bam_path))
    for interval in bed:
        chrom = clean_chrom(interval.chrom)
        if chrom not in get_chrom_list():
            continue
        start = interval.start - 1  # to correct for 0-based index
        end = interval.end - 1  # to correct for 0-based index
        file_obj.write("goto chr{0}:{1}-{2}\n".format(chrom, start, end))
        file_obj.write("sort strand\n")
        file_obj.write("collapse\n")
        file_obj.write("snapshot {0}_{1}_{2}.png\n".format(chrom, start, end))
    file_obj.write('exit')
    file_obj.close()
    return os.path.join(output_dir, 'igv_script.txt')


def read_is_low_quality(read, min_mapping_qual=20):
    """
    Check if the read passes few QC checks like MQ > mpq, no duplicate, mapped
    :param read: A pysam read object
    :param min_mapping_qual: Integer the minimum MQ mapping quality cutoff
    :return: Boolean
    """
    if read.is_duplicate:
        return True
    if read.is_qcfail:
        return True
    if read.is_unmapped:
        return True
    if read.mapq < min_mapping_qual:
        return True
    return False


def call_variants(bam, chrom, start, end, mpq=20, bcq=20):
    """
    Given a pysam BAM file object with a locus, get all variants in this interval along with read counts (all, +/-)
    that matche the mq and bq
    :param bam: Pysam BAM file object
    :param chrom: String
    :param start: Integer
    :param end: Integer
    :param mpq: Integer min read mapping quality
    :param bcq: Integer min base call quality
    :return: Dictionary of variants with their counts (all, +/-) on plus and minus strand
    """
    zero_index = 0  # padding if needed (only for SNV not INS or DEL)
    if start == end:  # snv
        zero_index = 1
        # No need to convert to 0-based index (since vcf file already does this for DEL/INS) but it is needed for snvs.

    reads = bam.fetch(chrom, start - zero_index, end)
    variants_holder = {}
    ref_counts = {'plus': 0, 'minus': 0}  # hq reads that support REF allele
    for read in reads:
        if read_is_low_quality(read, mpq):
            continue

        md_string = caller.parse_md(caller.get_md(read), [])
        bases = caller.make_base_seq(read.qname, read.query, read.qqual)
        try:
            observed_variants = caller.read_variants(qualthresh=bcq, chrom=chrom, read=read,
                                                     aligned_bases=bases, md_string=md_string)
        except:  # pylint: disable=W0702
            # TODO think of more specific exceptions
            continue

        # store the positions of all variants called in this read to check if it supports the REF allele or not
        read_variant_positions = []
        for variant in observed_variants:
            read_variant_positions.append(variant.pos)
            if variant.filter_reason is not None:
                continue
            if variant not in variants_holder:
                variants_holder[variant] = variant
            variants_holder[variant].raw_count += 1
            if read.is_reverse:
                variants_holder[variant].minus_count += 1
            else:
                variants_holder[variant].plus_count += 1

        # Does it support the REF allele (i.e. no variant overlap the start position?)
        if start not in read_variant_positions:
            if not any(['H' in read.cigarstring, 'S' in read.cigarstring]):
                if read.is_reverse:
                    # Our naive HTQC variant caller is not soft 'S' or hard 'H' clipping aware. So we need to ignore
                    # reads that have S or H clipping. In the future, we may need to calculate precisely where S/H
                    # clipping and allow pass read as long as the REF locus is fine. My general assumption here is reads
                    # with S/H clipping are great reads to start with, so I treat them as if they are low quality reads.
                    ref_counts['minus'] += 1
                else:
                    ref_counts['plus'] += 1
    return variants_holder, ref_counts


def get_variant_coverage(bam, chrom, pos, ref, alt):
    """
    Given an indel variant from VCF, get the coverage form the BAM file.
    See this URL for details
    https://bitbucket.org/mghcid/cider/wiki/HTQC%20fields%20in%20CIDer%20VCF%20files
    :param bam:
    :param chrom:
    :param pos:
    :param ref:
    :param alt:
    :return:
    """
    start = int(pos)
    end = start + abs(len(ref) - len(alt))  # define the genomic interval of the variant (i.e. indel size)
    variants, ref_counts = call_variants(bam, chrom, start, end, mpq=20, bcq=20)
    counts = {'ref': [0, 0, 0], 'alt': [0, 0, 0]}
    if end == start:
        pos -= 1  # if snvs, decrease by 1 to match the VCF POS
    for variant in variants:
        if variant.chrom == chrom and variant.vcf_pos == pos and variant.ref_seq == ref and variant.alt_seq == alt:
            counts['alt'][0] = variant.plus_count + variant.minus_count
            counts['alt'][1] = variant.plus_count
            counts['alt'][2] = variant.minus_count

            counts['ref'][0] = ref_counts['plus'] + ref_counts['minus']
            counts['ref'][1] = ref_counts['plus']
            counts['ref'][2] = ref_counts['minus']
    return counts
